import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MenuLogin {
  // ArrayList<ArrayList> daftarUser = new ArrayList<ArrayList>();
  List<DataUser> daftarUser = new ArrayList<>();

  int pilihan = 0;
  Scanner scanner = new Scanner(System.in);
  boolean isError = false;

  public boolean Welcome() {
    System.out.println("\nSelamat Datang di Agoda!");
    System.out.println("Silahkan Login atau Sign Up.");
    System.out.println("1) Login.");
    System.out.println("2) Sign Up.");
    System.out.print("\nPilih (1/2): ");
    pilihan = scanner.nextInt();
    scanner.nextLine();
    if (pilihan == 1) {
      if (Login()) {
        return true;
      } else {
        Welcome();
      }
    } else if (pilihan == 2) {
      SignUp();
    } else {
      System.out.println("Pilihan Salah!");
      return false;
    }
    return false;
  }

  boolean Login() {
    String email = "", password = "";
    System.out.print("\nEmail: ");
    email = scanner.nextLine();
    System.out.print("Password: ");
    password = scanner.nextLine();

    // ArrayList dataLogin = new ArrayList();
    // dataLogin.add(email);
    // dataLogin.add(password);

    // Cek Login
    for (DataUser dataUser : daftarUser) {
      if (dataUser.getEmail().equals(email) && dataUser.getPassword().equals(password)) {
        return true;
      }
    }
    System.out.println("Email / Password salah!");
    return false;
    //

    /*
     * // Cek Login
     * daftarUser.forEach((n) -> {
     * if (n.get(0).equals(dataLogin.get(0))) {
     * if (n.get(1).equals(dataLogin.get(1))) {
     * System.out.println("\nLogin Berhasil.");
     * this.isError = false;
     * } else {
     * System.out.println("\nPassword Salah.\nSilahkan coba lagi.\n");
     * this.isError = true;
     * }
     * } else {
     * System.out.
     * println("\nEmail tidak terdaftar\nSilahkan daftar terlebih dahulu\n");
     * this.isError = true;
     * }
     * });
     * return !isError;
     * //
     */
  }

  void SignUp() {
    this.isError = false;
    String email = "", password = "";
    System.out.print("\nEmail: ");
    email = scanner.nextLine();
    System.out.print("Password: ");
    password = scanner.nextLine();

    // Cek email yang sudah terdaftar
    for (DataUser dataUser : daftarUser) {
      if (dataUser.getEmail().equals(email)) {
        this.isError = true;
      }
    }
    //

    if (!isError) {
      daftarUser.add(new DataUser(email, password));

      System.out.println("\n\nPendaftaran Berhasil! Silahkan Login.");
      Welcome();
    } else {
      SignUp();
    }

  }

  public void generateDataUser() {
    // Generate data user [email, password]
    daftarUser.add(new DataUser("dummy@dummy.com", "dummyaja"));
    daftarUser.add(new DataUser("ayyhanifah@gmail.com", "1230046"));

  }
}

class DataUser {
  private String email;
  private String password;

  public DataUser(String email, String password) {
    this.email = email;
    this.password = password;
  }

  public String getEmail() {
    return email;
  }

  public String getPassword() {
    return password;
  }
}
