import java.util.Scanner;

public class MenuUtama {
  int pilihan = 0;
  Scanner scanner = new Scanner(System.in);

  int Menu() {
    System.out.println("\nSelamat Datang di Agoda!");
    System.out.println("Silahkan Pilih Menu.");
    System.out.println("1) Cari Hotel.");
    System.out.println("2) Cari Penginapan Pribadi.");
    System.out.println("3) Cari Tiket Pesawat.");
    System.out.println("4) Keluar Aplikasi.");

    System.out.print("\nPilih (1/2/3/4): ");
    pilihan = scanner.nextInt();
    scanner.nextLine();
    if (pilihan >= 1 && pilihan <= 4) {
      return pilihan;
    } else {
      System.out.println("\nMenu tidak ada.");
      return 0;
    }
  }
}
