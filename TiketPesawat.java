import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class TiketPesawat {
  List<DataTiket> daftarTiket = new ArrayList<>();
  Scanner scanner = new Scanner(System.in);
  String darimana = "", kemana = "";
  int tiket_dewasa = 0, tiket_anak = 0, tiket_bayi = 0;
  String tgl_pergi = "", tgl_pulang = "";
  int jenis_perjalanan = 0; // 1 - Satu Arah | 2 - Pulang Pergi
  int tipe_perjalanan = 0;
  int pencarian_Tiket = 0;

  public int CariTiket() throws ParseException {
    String pilihan = "";
    int total_harga = 0;

    pencarian_Tiket = 0;
    System.out.println("\n[Cari Tiket]");
    System.out.println("1. Satu Arah");
    System.out.println("2. Pulang Pergi");
    System.out.print("Jenis Perjalanan (1/2): ");
    jenis_perjalanan = scanner.nextInt();
    scanner.nextLine();

    System.out.print("Dari mana?: ");
    darimana = scanner.nextLine();

    System.out.print("Ke mana?: ");
    kemana = scanner.nextLine();

    System.out.print("Tanggal Pergi (contoh: 20/05/2023): ");
    tgl_pergi = scanner.nextLine();
    if (jenis_perjalanan == 2) {
      System.out.print("Tanggal Tanggal Pulang (contoh: 20/05/2023): ");
      tgl_pulang = scanner.nextLine();
    }
    // (1 - Economy | 2 - Bussiness | 3 - First Class | 4 - Premium Economy),
    System.out.println("1. Economy");
    System.out.println("2. Business");
    System.out.println("3. First Class");
    System.out.println("4. Premium Economy");

    System.out.print("Tipe Perjalanan (1/2/3/4): ");
    tipe_perjalanan = scanner.nextInt();
    scanner.nextLine();

    DataTiket queryPencarian = new DataTiket(0, "", jenis_perjalanan, darimana, kemana, tgl_pergi, tgl_pulang,
        tipe_perjalanan, 0, 0, 0);

    DataTiket hasilPencarian = PencarianTiket(queryPencarian);

    if (hasilPencarian != null) {
      System.out.println("\nTiket ditemukan");
      System.out.println("Harga Tiket Dewasa: Rp." + hasilPencarian.getHargaDewasa());
      System.out.println("Harga Tiket Anak: Rp." + hasilPencarian.getHargaAnak());
      System.out.println("Harga Tiket Bayi: Rp." + hasilPencarian.getHargaBayi());

      System.out.print("Apakah anda ingin melakukan booking? (Y/T): ");
      pilihan = scanner.nextLine();
      if (pilihan.equals("Y")) {

        System.out.print("Jumlah Tiket Dewasa: ");
        tiket_dewasa = scanner.nextInt();
        scanner.nextLine();
        System.out.print("Jumlah Tiket Bayi: ");
        tiket_bayi = scanner.nextInt();
        scanner.nextLine();
        System.out.print("Jumlah Tiket Anak: ");
        tiket_anak = scanner.nextInt();
        scanner.nextLine();

        System.out.println("\nBooking berhasil!\n");
        System.out.println("[RINCIAN BOOKING]");
        System.out.println("Nama Pesawat: " + hasilPencarian.getNama());
        System.out.println("Dari: " + darimana);
        System.out.println("Ke: " + kemana);
        System.out.println("Tanggal Pergi: " + tgl_pergi);
        if (jenis_perjalanan == 2) {
          System.out.println("Tanggal Pulang: " + tgl_pulang);
        }
        String tipe = "";
        int harga_dewasa = 0;
        int harga_anak = 0;
        int harga_bayi = 0;

        if (tipe_perjalanan == 1) {
          tipe = "Economy";
        } else if (tipe_perjalanan == 2) {
          tipe = "Business";
        } else if (tipe_perjalanan == 3) {
          tipe = "First Class";
        } else if (tipe_perjalanan == 3) {
          tipe = "Premium Economy";
        }
        harga_dewasa = hasilPencarian.getHargaDewasa();
        harga_anak = hasilPencarian.getHargaAnak();
        harga_bayi = hasilPencarian.getHargaBayi();

        total_harga = (harga_dewasa * tiket_dewasa) + (harga_anak * tiket_anak) + (harga_bayi * tiket_bayi);

        System.out.println("Tipe Perjalanan: " + tipe);
        if (tiket_dewasa > 0) {
          System.out.println("Harga Tiket Dewasa: Rp." + harga_dewasa + " x " + tiket_dewasa);
        }
        if (tiket_anak > 0) {
          System.out.println("Harga Tiket Anak: Rp." + harga_anak + " x " + tiket_anak);
        }
        if (tiket_bayi > 0) {
          System.out.println("Harga Tiket Bayi: Rp." + harga_bayi + " x " + tiket_bayi);
        }
        System.out.println("Total Harga: Rp." + total_harga + "\n");
        System.out.print("Tambah Hotel Lebih Hemat 25%! \nApakah anda ingin tambah hotel? (Y/T): ");
        pilihan = scanner.nextLine();
        if (pilihan.equals("Y")) {
          return 2;
        }
        System.out.print("Kembali ke menu Awal? (Y/T): ");
        pilihan = scanner.nextLine();
        if (pilihan.equals("T")) {
          return 0;
        }

      } else {
        System.out.println("Booking dibatalkan.");
        System.out.print("Kembali ke menu Awal? (Y/T): ");
        pilihan = scanner.nextLine();
        if (pilihan.equals("T")) {
          return 0;
        }

      }
    } else {
      System.out.println("\nTiket tidak ditemukan.");
      System.out.print("Kembali ke menu Awal? (Y/T): ");
      pilihan = scanner.nextLine();
      if (pilihan.equals("T")) {
        return 0;
      }
    }
    return 1;
  }

  DataTiket PencarianTiket(DataTiket queryPencarian) {
    for (DataTiket dataTiket : daftarTiket) {
      if (dataTiket.search(queryPencarian)) {
        return dataTiket;
      }
    }
    return null;
  }

  void generateDataTiket() {

    // Tambah data Tiket dengan urutan data sebagai berikut:
    /*
     * id,
     * nama pesawat,
     * jenis perjalanan (1 - Satu Arah | 2 - Pulang Pergi),
     * darimana,
     * kemana,
     * tanggal_pergi
     * tanggal_pulang,
     * tipe (1 - Economy | 2 - Bussiness | 3 - First Class | 4 - Premium Economy),
     * harga tiket dewasa,
     * harga tiket anak,
     * harga tiket bayi,
     */
    daftarTiket
        .add(new DataTiket(1, "Bandung - Bali SA Economy", 1, "Bandung", "Bali", "13/05/2023", "", 1, 500000, 250000,
            100000));
    daftarTiket
        .add(new DataTiket(2, "Bandung - Bali SA First Class", 1, "Bandung", "Bali", "13/05/2023", "", 3, 700000,
            350000, 150000));
    daftarTiket
        .add(new DataTiket(3, "Bandung - Bali PP First Class", 2, "Bandung", "Bali", "13/05/2023", "17/05/2023", 3,
            1000000, 500000, 250000));
    daftarTiket
        .add(new DataTiket(4, "Jakarta - Bali SA Economy", 1, "Jakarta", "Bali", "13/05/2023", "", 1, 600000, 300000,
            150000));
    daftarTiket
        .add(new DataTiket(5, "Jakarta - Bali SA First Class", 1, "Jakarta", "Bali", "13/05/2023", "", 3, 800000,
            400000, 200000));
    daftarTiket
        .add(new DataTiket(6, "Jakarta - Bali PP First Class", 2, "Jakarta", "Bali", "13/05/2023", "17/05/2023", 3,
            1100000, 550000, 250000));

  }

}

class DataTiket extends Tiket{
  private int id;
  private String nama;
  private int jenis_perjalanan;
  private String darimana;
  private String kemana;
  private String tanggal_pergi;
  private String tanggal_pulang;
  private int tipe;
  private int harga_dewasa;
  private int harga_anak;
  private int harga_bayi;

  public DataTiket(int id, String nama, int jenis_perjalanan, String darimana, String kemana,
      String tanggal_pergi,
      String tanggal_pulang, int tipe, int harga_dewasa, int harga_anak, int harga_bayi) {
    this.id = id;
    this.nama = nama;
    this.jenis_perjalanan = jenis_perjalanan;
    this.darimana = darimana;
    this.kemana = kemana;
    this.tanggal_pergi = tanggal_pergi;
    this.tanggal_pulang = tanggal_pulang;
    this.tipe = tipe;
    this.harga_dewasa = harga_dewasa;
    this.harga_anak = harga_anak;

    this.harga_bayi = harga_bayi;

  }

  public int getId() {
    return id;
  }

  public String getNama() {
    return nama;
  }

  public int getJenisPerjalanan() {
    return jenis_perjalanan;
  }

  public String getDarimana() {
    return darimana;
  }

  public String getKemana() {
    return kemana;
  }

  public String getTanggalPergi() {
    return tanggal_pergi;
  }

  public String getTanggalPulang() {
    return tanggal_pulang;
  }

  public int getTipe() {
    return tipe;
  }

  public int getHargaDewasa() {
    return harga_dewasa;
  }

  public int getHargaAnak() {
    return harga_anak;
  }

  public int getHargaBayi() {
    return harga_bayi;
  }

  public boolean search(Object obj) {
    DataTiket dataTiket = (DataTiket) obj;

    if (dataTiket.getJenisPerjalanan() == 2) {
      return this.jenis_perjalanan == dataTiket.getJenisPerjalanan() && this.darimana.equals(dataTiket.getDarimana())
          && this.kemana.equals(dataTiket.getKemana()) && this.tanggal_pergi.equals(dataTiket.getTanggalPergi())
          && this.tanggal_pulang.equals(dataTiket.getTanggalPulang()) && this.tipe == dataTiket.getTipe();
    } else {
      return this.jenis_perjalanan == dataTiket.getJenisPerjalanan() && this.darimana.equals(dataTiket.getDarimana())
          && this.kemana.equals(dataTiket.getKemana()) && this.tanggal_pergi.equals(dataTiket.getTanggalPergi())
          && this.tipe == dataTiket.getTipe();
    }

  }
}