import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Hotel {
  List<DataHotel> daftarHotel = new ArrayList<>();
  Scanner scanner = new Scanner(System.in);
  String lokasi = "";
  int kamar_dewasa = 0, kamar_anak = 0, kamar_bayi = 0, kamar_keluarga = 0;
  String tgl_checkin = "", tgl_checkout = "";
  int waktu_booking = 0; // 1 - Permalam | 2 - Perjam
  int jam_inap = 0;
  int tipe_kamar = 0;
  int pencarian_hotel = 0;

  public boolean CariHotel(boolean diskon_hotel) throws ParseException {
    String pilihan = "";
    int total_harga = 0;

    pencarian_hotel = 0;
    System.out.println("\n[Cari Hotel]");
    System.out.print("\nLokasi Hotel: ");
    lokasi = scanner.nextLine();
    System.out.println("1. Permalam");
    System.out.println("2. Perjam");
    System.out.print("Waktu Booking (1/2): ");
    waktu_booking = scanner.nextInt();
    scanner.nextLine();
    System.out.print("Tanggal Check-in (contoh: 20/05/2023): ");
    tgl_checkin = scanner.nextLine();
    if (waktu_booking == 1) {
      System.out.print("Tanggal Check-out (contoh: 20/05/2023): ");
      tgl_checkout = scanner.nextLine();
    } else {
      tgl_checkout = tgl_checkin;
      System.out.print("Jam penginapan (maks. 8 jam): ");
      jam_inap = scanner.nextInt();
      scanner.nextLine();
    }
    System.out.println("1. VIP");
    System.out.println("2. Tipe 1");
    System.out.println("3. Tipe 2");
    System.out.print("Tipe Kamar (1/2/3): ");
    tipe_kamar = scanner.nextInt();
    scanner.nextLine();
    System.out.print("Kamar Dewasa: ");
    kamar_dewasa = scanner.nextInt();
    scanner.nextLine();
    System.out.print("Kamar Bayi: ");
    kamar_bayi = scanner.nextInt();
    scanner.nextLine();
    System.out.print("Kamar Anak: ");
    kamar_anak = scanner.nextInt();
    scanner.nextLine();
    System.out.print("Kamar Keluarga: ");
    kamar_keluarga = scanner.nextInt();
    scanner.nextLine();

    DataHotel queryPencarian = new DataHotel(0, "", lokasi, tipe_kamar, 0, waktu_booking,
        kamar_dewasa, kamar_bayi, kamar_anak, kamar_keluarga);

    DataHotel hasilPencarian = PencarianHotel(queryPencarian);

    if (hasilPencarian != null) {
      System.out.print("\nHotel ditemukan.");
      if (waktu_booking == 1) {
        System.out.println("\nHarga Kamar Per-malam: Rp." + hasilPencarian.getHarga());
      } else {
        System.out.println("\nHarga Kamar Per-jam: Rp." + hasilPencarian.getHarga());
      }
      System.out.print("Apakah anda ingin melakukan booking? (Y/T): ");
      pilihan = scanner.nextLine();
      if (pilihan.equals("Y")) {
        System.out.println("Booking berhasil!\n");
        System.out.println("[RINCIAN BOOKING]");
        System.out.println("Nama Hotel: " + hasilPencarian.getNama());
        System.out.println("Lokasi Hotel: " + lokasi);
        System.out.println("Tanggal Check-in: " + tgl_checkin);
        System.out.println("Tanggal Check-out: " + tgl_checkout);
        String tipe = "";
        int harga = 0;
        if (tipe_kamar == 1) {
          tipe = "VIP";
        } else if (tipe_kamar == 2) {
          tipe = "Tipe 1";
        } else if (tipe_kamar == 3) {
          tipe = "Tipe 2";
        }
        harga = hasilPencarian.getHarga();
        System.out.println("Tipe Kamar: " + tipe);
        System.out.println("Kamar Dewasa: " + kamar_dewasa);
        System.out.println("Kamar Bayi: " + kamar_bayi);
        System.out.println("Kamar Anak: " + kamar_anak);
        System.out.println("Kamar Keluarga: " + kamar_keluarga);

        if (waktu_booking == 1) {
          Date checkin = new SimpleDateFormat("dd/MM/yyyy").parse(tgl_checkin);
          Date checkout = new SimpleDateFormat("dd/MM/yyyy").parse(tgl_checkout);
          long diffInMillies = Math.abs(checkout.getTime() - checkin.getTime());
          long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
          total_harga = harga * (int) diff;
          System.out.println("Harga Kamar: Rp." + harga);
          System.out.println("Waktu Penginapan: " + (int) diff + " Malam");
        } else {
          total_harga = harga * jam_inap;
          System.out.println("Harga Kamar: Rp." + harga);
          System.out.println("Waktu Penginapan: " + jam_inap + " Jam");
        }
        System.out.println("Total Harga: Rp." + total_harga + "\n");
        if (diskon_hotel) {
          System.out.println("Diskon Hotel (25%): Rp." + (total_harga * 25 / 100));
          total_harga = total_harga - (total_harga * 25 / 100);
          System.out.println("Harga Akhir : Rp." + total_harga + "\n");

        }
        System.out.print("Kembali ke menu Awal? (Y/T): ");
        pilihan = scanner.nextLine();
        if (pilihan.equals("T")) {
          return false;
        }

      } else {
        System.out.println("Booking dibatalkan.");
        System.out.print("Kembali ke menu Awal? (Y/T): ");
        pilihan = scanner.nextLine();
        if (pilihan.equals("T")) {
          return false;
        }

      }
    } else {
      System.out.println("\nHotel tidak ditemukan.");
      System.out.print("Kembali ke menu Awal? (Y/T): ");
      pilihan = scanner.nextLine();
      if (pilihan.equals("T")) {
        return false;
      }
    }
    return true;
  }

  DataHotel PencarianHotel(DataHotel queryPencarian) {
    for (DataHotel dataHotel : daftarHotel) {
      if (dataHotel.search(queryPencarian)) {
        return dataHotel;
      }
    }
    return null;
  }

  void generateDataHotel() {

    // Tambah data hotel dengan urutan data sebagai berikut:
    /*
     * id,
     * nama hotel,
     * lokasi,
     * tipe hotel (1 - VIP | 2 - Tipe 1 | 3 - Tipe 2),
     * harga (Harga per-malam / per-jam),
     * waktu booking (1 - Per-malam | 2 - Per-jam),
     * jumlah kamar dewasa,
     * jumlah kamar bayi,
     * jumlah kamar anak,
     * jumlah kamar keluarga
     */
    daftarHotel.add(new DataHotel(1, "Hotel Bandung VIP 1", "Bandung", 1, 50000, 1, 2, 0, 0, 0));
    daftarHotel.add(new DataHotel(1, "Hotel Bandung VIP 2", "Bandung", 1, 50000, 1, 2, 0, 1, 0));
    daftarHotel.add(new DataHotel(1, "Hotel Bandung Tipe 1", "Bandung", 2, 30000, 1, 2, 0, 0, 0));
    daftarHotel.add(new DataHotel(1, "Hotel Jakarta Tipe 2", "Jakarta", 3, 25000, 1, 1, 0, 0, 0));
    daftarHotel.add(new DataHotel(1, "Hotel Jakarta Perjam", "Jakarta", 3, 25000, 2, 1, 0, 0, 0));
    daftarHotel.add(new DataHotel(1, "Hotel Bali VIP 1", "Bali", 1, 50000, 1, 2, 0, 1, 0));

  }

}

class DataHotel extends Hotel{
  private int id;
  private String nama;
  private String lokasi;
  private int tipe;
  private int harga;
  private int waktu_booking;
  private int kamar_dewasa;
  private int kamar_anak;
  private int kamar_bayi;
  private int kamar_keluarga;

  public DataHotel(int id, String nama, String lokasi, int tipe, int harga, int waktu_booking,
      int kamar_dewasa,
      int kamar_bayi,
      int kamar_anak,
      int kamar_keluarga) {
    this.id = id;
    this.nama = nama;
    this.lokasi = lokasi;
    this.tipe = tipe;
    this.harga = harga;
    this.waktu_booking = waktu_booking;
    this.kamar_dewasa = kamar_dewasa;
    this.kamar_anak = kamar_anak;
    this.kamar_bayi = kamar_bayi;
    this.kamar_keluarga = kamar_keluarga;
  }

  public int getId() {
    return id;
  }

  public String getNama() {
    return nama;
  }

  public String getLokasi() {
    return lokasi;
  }

  public int getHarga() {
    return harga;
  }

  public int getTipe() {
    return tipe;
  }

  public int getWaktuBooking() {
    return waktu_booking;
  }

  public int getKamarDewasa() {
    return kamar_dewasa;
  }

  public int getKamarBayi() {
    return kamar_bayi;
  }

  public int getKamarAnak() {
    return kamar_anak;
  }

  public int getKamarKeluarga() {
    return kamar_keluarga;
  }

  public boolean search(Object obj) {
    DataHotel dataHotel = (DataHotel) obj;

    return this.lokasi.equals(dataHotel.getLokasi()) && this.tipe == dataHotel.getTipe()
        && this.waktu_booking == dataHotel.getWaktuBooking() && this.kamar_dewasa >= dataHotel.getKamarDewasa()
        && this.kamar_bayi >= dataHotel.getKamarBayi() && this.kamar_anak >= dataHotel.getKamarAnak()
        && this.kamar_keluarga >= dataHotel.getKamarKeluarga();
  }
}