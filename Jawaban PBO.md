# NO.1 
Algoritma dalam bentuk pseudecode:

procedure main():
    pilihan <- 0
    diskon_hotel <- false

    menuLogin <- new MenuLogin()
    menuUtama <- new MenuUtama()
    hotel <- new Hotel()
    penginapanPribadi <- new Penginapan()
    tiketPesawat <- new TiketPesawat()
    menuLogin.generateDataUser()
    hotel.generateDataHotel()
    penginapanPribadi.generateDataPenginapan()
    tiketPesawat.generateDataTiket()

    menuLogin.Welcome()

    while pilihan is not equal to 4:
        if not diskon_hotel then
            pilihan <- menuUtama.Menu()
        end if

        switch pilihan:
            case 1:
                if not hotel.CariHotel(diskon_hotel) then
                    pilihan <- 4
                end if
                diskon_hotel <- false
            case 2:
                if not penginapanPribadi.CariPenginapan() then
                    pilihan <- 4
                end if
            case 3:
                tambah_hotel <- tiketPesawat.CariTiket()
                if tambah_hotel is equal to 0 then
                    pilihan <- 4
                else if tambah_hotel is equal to 2 then
                    pilihan <- 1
                    diskon_hotel <- true
                end if
            case 4:
                print "Keluar Aplikasi."
            default:
                print "Menu tidak ada"
        end switch
    end while

end procedure

Berikut adalah programnya:
[Link](App.java)

# NO.2
Berikut penjelasan algoritma:

1. Inisialisasi variabel dan objek yang diperlukan.
2. Memanggil metode untuk menghasilkan data pengguna, data hotel, data penginapan, dan data tiket.
3. Menampilkan pesan selamat datang.
4. Memulai loop while dengan kondisi pilihan tidak sama dengan 4.
5. Jika diskon_hotel adalah false, tampilkan menu utama dan dapatkan pilihan dari pengguna.
6. Melakukan switch case berdasarkan pilihan yang diberikan.
7. Jika pilihan adalah 1, mencari hotel dengan memanggil metode CariHotel dari objek hotel. Jika hotel tidak ditemukan, pilihan diatur menjadi 4 (untuk keluar).
8. Mengatur diskon_hotel menjadi false.
9. Jika pilihan adalah 2, mencari penginapan dengan memanggil metode CariPenginapan dari objek penginapanPribadi. Jika penginapan tidak ditemukan, pilihan diatur menjadi 4 (untuk keluar).
10. Jika pilihan adalah 3, mencari tiket pesawat dengan memanggil metode CariTiket dari objek tiketPesawat. Jika tambah_hotel adalah 0, pilihan diatur menjadi 4 (untuk keluar). Jika tambah_hotel adalah 2, pilihan diatur menjadi 1 dan diskon_hotel diatur menjadi true.
11. Jika pilihan adalah 4, mencetak pesan "Keluar Aplikasi".
12. Jika pilihan tidak sesuai dengan kasus-kasus di atas, mencetak pesan "Menu tidak ada".
13. Mengulangi langkah 5-12 sampai pilihan adalah 4 (untuk keluar).

Algoritma tersebut memungkinkan pengguna untuk melakukan interaksi dengan menu yang ditampilkan dan menjalankan berbagai fungsi terkait hotel, penginapan, dan tiket pesawat.

Berikut adalah programnya:
[Link](App.java)
# NO.3
Mampu menjelaskan Konsep Dasar OOP.
- Kelas (Class)
Kelas adalah kumpulan objek-objek dengan
karakteristik yang sama. Kelas merupakan defenisi
statik dan himpunan objek yang sama mungkin lahir
atau diciptakan oleh kelas tersebut. Sebuah kelas
mempunyai sifat (atribut), kelakuan
(operasi/method), hubungan (relationship) dan arti.Suatu kelas dapat diturunkan kekelas yang lain,dimana atribut dan kelas semula dapat diwariskan kekelas yang baru.
- Objek (Object)
Objek adalah abstraksi dan sesuatu yang mewakili
dunia nyata seperti benda, manusia, satuan organisasi,tempat, kejadian, struktur, status, atau hal-hal lain yang bersifat abstrak. Objek merupakan suatu entitas yang mampu menyimpan informasi (status) dan mempunyai operasi (kelakuan) yang dapat diterapkan atau dapt brtpengaris pada statis objeknya. Objek mempunyai siklus hidup yaitu diciptakan,
dimanipulasi dan dihancurkan.
- Metode (Method)
Operasi atau metode atau method pada sebuah kelas
hampir sama dengan fungsi atau prosedur pada
metodologi struktural. Sebuah kelas boleh memiliki
lebih dari satu metode atau operasi. Metode atau
operasi yang berfungsi untuk memanipulasi objek itu sendiri. Operasi atau metode merupakan fungsi atau transformasi yang dapat dilakukan terhadap objek atau yang dilakukan oleh objek.
- Atribut (Attribute)
Atribut dari sebuah kelas adalah variabel global yang dimiliki sebuah kelas. Atribut dapat berupa nilai atau elemen-elemen data yang dimiliki oleh objek dalam kelas objek. Atribut dipunyai secara individual oleh sebuah objek, misalnya berat, jenis, nama, dan sebagainya. Atribut sebaiknya bersifat privat untuk menjaga enkasulapsi.

4 Pilar OOP adalah 4 konsep dasar dalam pemograman Berorientasi Objek (OOP) : 
- Enkapsulasi (Encapsulation):
Enkapsulasi melibatkan penggabungan data (atribut) dan fungsi (metode) terkait dalam satu unit yang disebut objek. Tujuan enkapsulasi adalah menyembunyikan detail implementasi dan membatasi akses langsung ke data dari luar objek. Ini dicapai dengan menggunakan aksesibilitas (misalnya, public, private, protected) dan penggunaan metode getter dan setter untuk mengatur dan mengakses atribut objek dengan cara yang terkontrol. Enkapsulasi membantu dalam menjaga integritas data, mencegah perubahan langsung pada data, dan mempermudah perubahan implementasi tanpa mempengaruhi kode yang menggunakannya.
- Pewarisan (Inheritance):
Pewarisan adalah mekanisme di mana sebuah kelas baru (kelas turunan atau subclass) dapat mewarisi atribut dan metode dari kelas yang ada (kelas induk atau superclass). Dengan menggunakan pewarisan, kelas turunan dapat memperluas atau mengkhususkan perilaku dan atribut yang sudah ada dalam kelas induk. Pewarisan memungkinkan untuk membangun hierarki kelas, menerapkan konsep "satu aturan yang berlaku untuk semuanya", dan memungkinkan penggunaan kembali kode yang sudah ada. Dalam pewarisan, konsep "is-a" digunakan untuk menyatakan hubungan antara kelas turunan dan kelas induk.
- Abstraksi (Abstraction):
Abstraksi melibatkan penyederhanaan kompleksitas dengan mengidentifikasi atribut dan metode yang paling relevan untuk sebuah objek dan mengabaikan detail yang tidak penting. Ini melibatkan penentuan karakteristik dan perilaku utama objek yang perlu diperhatikan dalam model. Abstraksi membantu dalam memodelkan dunia nyata dengan representasi yang lebih sederhana, mengurangi kompleksitas dan ketergantungan pada detail implementasi, dan mempermudah pemahaman dan penggunaan objek oleh pengembang lain.
- Polimorfisme (Polymorphism):
Polimorfisme memungkinkan objek dengan tipe yang berbeda untuk merespons metode yang sama dengan cara yang berbeda. Dalam OOP, ini dapat dicapai melalui penggunaan metode yang memiliki nama yang sama, tetapi memiliki implementasi yang berbeda di kelas yang berbeda. Polimorfisme memungkinkan pemrosesan objek secara umum, tanpa harus mengetahui tipe spesifik objek saat kompilasi. Ini meningkatkan fleksibilitas, modularitas, dan reusabilitas kode.

# NO.4
Enkapsulasi (Encapsulation):
Enkapsulasi melibatkan penggabungan data (atribut) dan fungsi (metode) terkait dalam satu unit yang disebut objek. Tujuan enkapsulasi adalah menyembunyikan detail implementasi dan membatasi akses langsung ke data dari luar objek. Ini dicapai dengan menggunakan aksesibilitas (misalnya, public, private, protected) dan penggunaan metode getter dan setter untuk mengatur dan mengakses atribut objek dengan cara yang terkontrol. Enkapsulasi membantu dalam menjaga integritas data, mencegah perubahan langsung pada data, dan mempermudah perubahan implementasi tanpa mempengaruhi kode yang menggunakannya.

Implementasi enkapsulasi dalam program yang saya buat dapat dilihat dalam beberapa aspek:

1. Kelas dan metode memiliki tingkat akses yang tepat: Kelas-kelas seperti `MenuLogin`, `MenuUtama`, `Hotel`, `Penginapan`, dan `TiketPesawat` memiliki tingkat akses publik sehingga dapat diakses dari kelas lain. Namun, variabel `pilihan` dan `diskon_hotel` memiliki tingkat akses private sehingga hanya dapat diakses di dalam kelas `App`.

2. Penggunaan metode akses (getter dan setter): Tidak ada penggunaan langsung metode akses dalam program ini, namun, dalam kelas-kelas lain seperti `Hotel`, `Penginapan`, dan `TiketPesawat`, kemungkinan ada penggunaan metode akses (getter dan setter) untuk mengakses dan memanipulasi data-data yang ada di dalam kelas tersebut. Hal ini membantu dalam melindungi data dan membatasi akses langsung ke variabel-variabel yang terkait.

3. Penggunaan konstruktor: Konstruktor digunakan dalam pembuatan objek-objek seperti `MenuLogin`, `MenuUtama`, `Hotel`, `Penginapan`, dan `TiketPesawat`. Konstruktor ini dapat menerima argumen dan menginisialisasi objek dengan nilai-nilai awal yang sesuai.

4. Penggunaan metode publik: Metode-metode publik yang ada dalam kelas-kelas seperti `MenuLogin`, `MenuUtama`, `Hotel`, `Penginapan`, dan `TiketPesawat` dapat diakses dari luar kelas `App`. Namun, implementasi internal dari metode-metode ini tersembunyi dan tidak dapat diakses secara langsung dari luar kelas.

Dengan menggunakan enkapsulasi, program ini dapat mengorganisir data dan fungsionalitas yang terkait dalam kelas-kelas yang terpisah, memungkinkan akses yang terkontrol ke data dan mempromosikan pemisahan antara implementasi internal dan penggunaan eksternal.

Berikut Programnya:
[class Menu Utama](https://gitlab.com/euisnurhaa18/pemograman-berorientasi-objek/-/blob/main/MenuUtama.java)
[class Menu Login](https://gitlab.com/euisnurhaa18/pemograman-berorientasi-objek/-/blob/main/MenuLogin.java)
[class Hotel](https://gitlab.com/euisnurhaa18/pemograman-berorientasi-objek/-/blob/main/Hotel.java)
[class Penginapan](https://gitlab.com/euisnurhaa18/pemograman-berorientasi-objek/-/blob/main/Penginapan.java)
[Class Tiket Pesawat](https://gitlab.com/euisnurhaa18/pemograman-berorientasi-objek/-/blob/main/TiketPesawat.java)

# NO.5
Abstraksi dalam konteks program di atas mengacu pada pemisahan antara implementasi detail yang kompleks dengan representasi yang lebih sederhana dan terfokus pada fungsionalitas yang relevan. Berikut adalah beberapa contoh abstraksi dalam program tersebut:

1. Kelas `MenuLogin`, `MenuUtama`, `Hotel`, `Penginapan`, dan `TiketPesawat` merupakan abstraksi dari entitas yang terkait dengan login, menu utama, hotel, penginapan, dan tiket pesawat. Masing-masing kelas ini berfungsi sebagai entitas yang terpisah dan mengelola operasi-operasi terkait dengan entitas yang mewakilinya. Mereka menyediakan antarmuka yang sederhana dan terfokus, yang memungkinkan penggunaan fungsionalitas tersebut tanpa perlu memahami detail implementasinya.

2. Metode `generateDataUser()`, `generateDataHotel()`, `generateDataPenginapan()`, dan `generateDataTiket()` merupakan abstraksi dari logika yang kompleks untuk menghasilkan data pengguna, data hotel, data penginapan, dan data tiket. Detail implementasi logika tersebut disembunyikan dari `App` dan disediakan melalui antarmuka yang lebih sederhana untuk menginisialisasi data yang diperlukan.

3. Metode `CariHotel()`, `CariPenginapan()`, dan `CariTiket()` merupakan abstraksi dari proses pencarian yang melibatkan logika dan manipulasi data yang kompleks. Dalam implementasi detailnya di kelas-kelas `Hotel`, `Penginapan`, dan `TiketPesawat`, logika tersebut diterapkan untuk mencari hotel, penginapan, dan tiket pesawat yang sesuai dengan kriteria yang diberikan. Namun, dalam `App`, penggunaan metode-metode ini hanya melibatkan pemanggilan dan pemrosesan hasilnya, tanpa perlu memahami detail implementasi pencariannya.

Melalui abstraksi, program tersebut dapat mengelompokkan fungsionalitas terkait ke dalam kelas-kelas yang terpisah, menyembunyikan detail implementasi yang kompleks, dan menyediakan antarmuka yang lebih sederhana dan terfokus. Hal ini memudahkan pemahaman, penggunaan, dan pemeliharaan program secara keseluruhan.

Berikut Programnya:
[class Menu Utama](https://gitlab.com/euisnurhaa18/pemograman-berorientasi-objek/-/blob/main/MenuUtama.java)
[class Menu Login](https://gitlab.com/euisnurhaa18/pemograman-berorientasi-objek/-/blob/main/MenuLogin.java)
[class Hotel](https://gitlab.com/euisnurhaa18/pemograman-berorientasi-objek/-/blob/main/Hotel.java)
[class Penginapan](https://gitlab.com/euisnurhaa18/pemograman-berorientasi-objek/-/blob/main/Penginapan.java)
[Class Tiket Pesawat](https://gitlab.com/euisnurhaa18/pemograman-berorientasi-objek/-/blob/main/TiketPesawat.java)

# NO.6 
Dalam program yang saya buat, konsep Inheritance (Pewarisan) dan Polymorphism (Polimorfisme) digunakan untuk mempermudah pengelolaan dan penggunaan kelas-kelas yang terlibat.

Inheritance (Pewarisan) adalah konsep di mana sebuah kelas dapat mewarisi properti dan metode dari kelas lain yang disebut kelas induk atau kelas dasar. Dalam program tersebut, terdapat contoh penggunaan inheritance dengan adanya kelas "DataHotel" yang merupakan kelas dasar, dan kelas "Hotel" yang merupakan kelas turunan. Kelas "Hotel" mewarisi properti dan metode dari kelas "DataHotel". Dengan menggunakan inheritance, kita dapat menggunakan kembali properti dan metode yang sudah ada tanpa perlu menuliskan ulang.

Polymorphism (Polimorfisme) adalah kemampuan sebuah objek untuk memiliki banyak bentuk. Dalam konteks program tersebut, polimorfisme diterapkan melalui penggunaan metode "search" dalam kelas "DataHotel". Metode ini menerima objek pencarian sebagai parameter dan mengembalikan hasil pencarian yang sesuai dengan objek tersebut. Dalam program tersebut, objek pencarian dapat berupa objek dari kelas "DataHotel" maupun objek dari kelas turunan seperti "Hotel". Dengan demikian, metode "search" dapat memproses objek-objek dengan tipe yang berbeda namun tetap memberikan hasil yang diharapkan.

Dengan menggunakan inheritance dan polimorfisme, program tersebut menjadi lebih fleksibel dan modular. Kita dapat dengan mudah menambahkan kelas turunan baru dengan properti dan metode tambahan tanpa mengganggu fungsionalitas yang ada. Selain itu, pemakaian polimorfisme memungkinkan kita untuk mengelola objek-objek dengan tipe yang berbeda secara seragam melalui metode yang sama.

Berikut Programnya:

[class Hotel](https://gitlab.com/euisnurhaa18/pemograman-berorientasi-objek/-/blob/main/Hotel.java)

# NO.7 

Dalam program yang saya buat, proses bisnis atau kumpulan use case dapat dideskripsikan ke dalam skema OOP dengan memodelkannya menjadi kelas-kelas yang mewakili entitas-entitas terkait dan metode-metode yang merepresentasikan aksi-aksi yang dilakukan dalam proses bisnis tersebut. Berikut adalah deskripsi berdasarkan skema OOP:

1. Kelas `App`:
   - Merupakan kelas utama yang mengontrol alur program dan berfungsi sebagai titik masuk.
   - Menggunakan objek-objek dari kelas-kelas lain untuk menjalankan aksi-aksi yang terkait dengan proses bisnis.
   - Menerapkan logika kontrol dengan perulangan dan switch case untuk mengatur aliran program berdasarkan pilihan pengguna.

2. Kelas `MenuLogin`:
   - Mewakili menu login dalam proses bisnis.
   - Memiliki metode `generateDataUser()` untuk menginisialisasi data pengguna.
   - Memiliki metode `Welcome()` untuk menampilkan pesan selamat datang.

3. Kelas `MenuUtama`:
   - Mewakili menu utama dalam proses bisnis.
   - Memiliki metode `Menu()` untuk menampilkan menu utama dan mengembalikan pilihan pengguna.

4. Kelas `Hotel`:
   - Mewakili entitas hotel dalam proses bisnis.
   - Memiliki metode `generateDataHotel()` untuk menginisialisasi data hotel.
   - Memiliki metode `CariHotel()` untuk mencari hotel berdasarkan kriteria tertentu.

5. Kelas `Penginapan`:
   - Mewakili entitas penginapan dalam proses bisnis.
   - Memiliki metode `generateDataPenginapan()` untuk menginisialisasi data penginapan.
   - Memiliki metode `CariPenginapan()` untuk mencari penginapan berdasarkan kriteria tertentu.

6. Kelas `TiketPesawat`:
   - Mewakili entitas tiket pesawat dalam proses bisnis.
   - Memiliki metode `generateDataTiket()` untuk menginisialisasi data tiket pesawat.
   - Memiliki metode `CariTiket()` untuk mencari tiket pesawat berdasarkan kriteria tertentu.

Dalam skema OOP, setiap kelas mewakili entitas atau konsep yang relevan dalam proses bisnis, sementara metode-metode dalam setiap kelas merepresentasikan aksi-aksi yang terkait dengan entitas tersebut. Dengan memodelkan proses bisnis menjadi kelas-kelas dengan perilaku dan atribut yang sesuai, program dapat lebih terstruktur, modular, dan dapat diubah dengan lebih mudah sesuai kebutuhan bisnis yang berubah.

# NO.8
Penjelasan class diagram:

1. Class `App`:
   - Kelas utama yang berisi metode `main` sebagai titik masuk program.
   - Memiliki atribut `pilihan` untuk menyimpan pilihan pengguna dan `diskon_hotel` untuk menyimpan status diskon hotel.
   - Membuat objek-objek dari kelas `MenuLogin`, `MenuUtama`, `Hotel`, `Penginapan`, dan `TiketPesawat`.
   - Memanggil metode `generateDataUser()`, `generateDataHotel()`, `generateDataPenginapan()`, dan `generateDataTiket()` untuk menginisialisasi data yang diperlukan.
   - Memanggil metode `Welcome()` dari objek `menuLogin` untuk menampilkan pesan selamat datang.
   - Melakukan perulangan while hingga `pilihan` sama dengan 4.
   - Mengecek kondisi `diskon_hotel` dan memanggil metode `Menu()` dari objek `menuUtama` untuk mendapatkan pilihan pengguna.
   - Menggunakan switch case untuk mengeksekusi aksi yang sesuai berdasarkan pilihan pengguna:
     - Pilihan 1: Memanggil metode `CariHotel()` dari objek `hotel` dengan parameter `diskon_hotel`.
     - Pilihan 2: Memanggil metode `CariPenginapan()` dari objek `penginapanPribadi`.
     - Pilihan 3: Memanggil metode `CariTiket()` dari objek `tiketPesawat` dan menyimpan hasilnya dalam variabel `tambah_hotel`.
     - Pilihan 4: Menampilkan pesan keluar aplikasi.
     - Default: Menampilkan pesan bahwa menu tidak ada.
   - Kelas ini tidak terlihat memiliki hubungan dengan kelas-kelas lain dalam program.

2. Class `MenuLogin`:
   - Kelas yang memiliki metode `generateDataUser()` untuk menginisialisasi data pengguna dan metode `Welcome()` untuk menampilkan pesan selamat datang.

3. Class `MenuUtama`:
   - Kelas yang memiliki metode `Menu()` untuk menampilkan menu utama dan mengembalikan pilihan pengguna.

4. Class `Hotel`:
   - Kelas yang memiliki metode `generateDataHotel()` untuk menginisialisasi data hotel dan metode `CariHotel()` untuk mencari hotel berdasarkan kriteria tertentu.

5. Class `Penginapan`:
   - Kelas yang memiliki metode `generateDataPenginapan()` untuk menginisialisasi data penginapan dan metode `CariPenginapan()` untuk mencari penginapan berdasarkan kriteria tertentu.

6. Class `TiketPesawat`:
   - Kelas yang memiliki metode `generateDataTiket()` untuk menginisialisasi data tiket pesawat dan metode `CariTiket()` untuk mencari tiket pesawat berdasarkan kriteria tertentu.

   Class Diagram : 
   [Class Diagram](Diagram_Class.png)
   Use case : 
   [Use Case](Use_Case.pdf)

# No.9
https://youtu.be/Ru6rhZQT38Q 
# NO.10
[Output program](Output.pdf)
