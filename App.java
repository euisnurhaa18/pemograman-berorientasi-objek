public class App {
    public static void main(String[] args) throws Exception {
        int pilihan = 0;
        boolean diskon_hotel = false;

        MenuLogin menuLogin = new MenuLogin();
        MenuUtama menuUtama = new MenuUtama();
        Hotel hotel = new Hotel();  
        Penginapan penginapanPribadi = new Penginapan();
        TiketPesawat tiketPesawat = new TiketPesawat();
        menuLogin.generateDataUser();
        hotel.generateDataHotel();
        penginapanPribadi.generateDataPenginapan();
        tiketPesawat.generateDataTiket();

        menuLogin.Welcome();

        while (pilihan != 4) {
            if (!diskon_hotel) {
                pilihan = menuUtama.Menu();
            }
            switch (pilihan) {
                case 1:
                    if (!hotel.CariHotel(diskon_hotel)) {
                        pilihan = 4;
                    }
                    diskon_hotel = false;
                    break;
                case 2:
                    if (!penginapanPribadi.CariPenginapan()) {
                        pilihan = 4;
                    }
                    break;
                case 3:
                    int tambah_hotel = tiketPesawat.CariTiket();
                    if (tambah_hotel == 0) {
                        pilihan = 4;
                    } else if (tambah_hotel == 2) {
                        pilihan = 1;
                        diskon_hotel = true;
                    }
                    break;
                case 4:
                    System.out.println("\nKeluar Aplikasi.");
                    break;
                default:
                    System.out.println("\nMenu tidak ada");
                    break;
            }
        }

    }
}