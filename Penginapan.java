import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Penginapan {
  List<DataPenginapan> daftarPenginapan = new ArrayList<>();
  Scanner scanner = new Scanner(System.in);
  String lokasi = "";
  int kamar_dewasa = 0, kamar_anak = 0, kamar_bayi = 0, kamar_keluarga = 0;
  String tgl_checkin = "", tgl_checkout = "";
  int jam_inap = 0;
  int tipe_kamar = 0;
  int pencarian_Penginapan = 0;

  public boolean CariPenginapan() throws ParseException {
    String pilihan = "";
    int total_harga = 0;

    pencarian_Penginapan = 0;
    System.out.println("\n[Cari Penginapan]");
    System.out.print("\nLokasi Penginapan: ");
    lokasi = scanner.nextLine();
    System.out.print("Tanggal Check-in (contoh: 20/05/2023): ");
    tgl_checkin = scanner.nextLine();
    System.out.print("Tanggal Check-out (contoh: 20/05/2023): ");
    tgl_checkout = scanner.nextLine();
    System.out.println("1. Tipe Akomodasi");
    System.out.println("2. Akomodasi Unik");
    System.out.print("Tipe Kamar (1/2): ");
    tipe_kamar = scanner.nextInt();
    scanner.nextLine();
    System.out.print("Kamar Dewasa: ");
    kamar_dewasa = scanner.nextInt();
    scanner.nextLine();
    System.out.print("Kamar Bayi: ");
    kamar_bayi = scanner.nextInt();
    scanner.nextLine();
    System.out.print("Kamar Anak: ");
    kamar_anak = scanner.nextInt();
    scanner.nextLine();
    System.out.print("Kamar Keluarga: ");
    kamar_keluarga = scanner.nextInt();
    scanner.nextLine();

    DataPenginapan queryPencarian = new DataPenginapan(0, "", lokasi, tipe_kamar, 0,
        kamar_dewasa, kamar_bayi, kamar_anak, kamar_keluarga);

    DataPenginapan hasilPencarian = PencarianPenginapan(queryPencarian);

    if (hasilPencarian != null) {
      System.out.print("\nPenginapan ditemukan.");
      System.out.println("\nHarga  Per-malam: Rp." + hasilPencarian.getHarga());
      System.out.print("Apakah anda ingin melakukan booking? (Y/T): ");
      pilihan = scanner.nextLine();
      if (pilihan.equals("Y")) {
        System.out.println("Booking berhasil!\n");
        System.out.println("[RINCIAN BOOKING]");
        System.out.println("Nama Penginapan: " + hasilPencarian.getNama());
        System.out.println("Lokasi Penginapan: " + lokasi);
        System.out.println("Tanggal Check-in: " + tgl_checkin);
        System.out.println("Tanggal Check-out: " + tgl_checkout);
        String tipe = "";
        int harga = 0;
        if (tipe_kamar == 1) {
          tipe = "Tipe Akomodasi";
        } else if (tipe_kamar == 2) {
          tipe = "Akomodasi Unik";
        }
        harga = hasilPencarian.getHarga();
        System.out.println("Tipe Kamar: " + tipe);
        System.out.println("Kamar Dewasa: " + kamar_dewasa);
        System.out.println("Kamar Bayi: " + kamar_bayi);
        System.out.println("Kamar Anak: " + kamar_anak);
        System.out.println("Kamar Keluarga: " + kamar_keluarga);

        Date checkin = new SimpleDateFormat("dd/MM/yyyy").parse(tgl_checkin);
        Date checkout = new SimpleDateFormat("dd/MM/yyyy").parse(tgl_checkout);
        long diffInMillies = Math.abs(checkout.getTime() - checkin.getTime());
        long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
        total_harga = harga * (int) diff;
        System.out.println("Harga Penginapan: Rp." + harga);
        System.out.println("Waktu Penginapan: " + (int) diff + " Malam");
        System.out.println("Total Harga: Rp." + total_harga + "\n");
        System.out.print("Kembali ke menu Awal? (Y/T): ");
        pilihan = scanner.nextLine();
        if (pilihan.equals("T")) {
          return false;
        }

      } else {
        System.out.println("Booking dibatalkan.");
        System.out.print("Kembali ke menu Awal? (Y/T): ");
        pilihan = scanner.nextLine();
        if (pilihan.equals("T")) {
          return false;
        }

      }
    } else {
      System.out.println("\nPenginapan tidak ditemukan.");
      System.out.print("Kembali ke menu Awal? (Y/T): ");
      pilihan = scanner.nextLine();
      if (pilihan.equals("T")) {
        return false;
      }
    }
    return true;

  }

  DataPenginapan PencarianPenginapan(DataPenginapan queryPencarian) {
    for (DataPenginapan dataPenginapan : daftarPenginapan) {
      if (dataPenginapan.search(queryPencarian)) {
        return dataPenginapan;
      }
    }
    return null;
  }

  void generateDataPenginapan() {

    // Tambah data Penginapan dengan urutan data sebagai berikut:
    /*
     * id,
     * nama Penginapan,
     * lokasi,
     * tipe Penginapan (1 - Tipe Akomodasi | 2 - Akomodasi Unik),
     * harga (Harga per-malam),
     * jumlah kamar dewasa,
     * jumlah kamar bayi,
     * jumlah kamar anak,
     * jumlah kamar keluarga
     */
    daftarPenginapan.add(new DataPenginapan(1, "Penginapan Bandung Akomodasi 1", "Bandung", 1, 30000, 2, 0, 0, 0));
    daftarPenginapan.add(new DataPenginapan(1, "Penginapan Bandung Akomodasi 2", "Bandung", 1, 30000, 2, 0, 1, 0));
    daftarPenginapan.add(new DataPenginapan(1, "Penginapan Bandung Unik 1", "Bandung", 2, 20000, 2, 0, 0, 0));
    daftarPenginapan.add(new DataPenginapan(1, "Penginapan Jakarta Akomodasi 1", "Jakarta", 1, 20000, 1, 0, 0, 0));
    daftarPenginapan.add(new DataPenginapan(1, "Penginapan Jakarta Unik 1", "Jakarta", 2, 30000, 1, 0, 0, 0));
  }

}

class DataPenginapan {
  private int id;
  private String nama;
  private String lokasi;
  private int tipe;
  private int harga;
  private int kamar_dewasa;
  private int kamar_anak;
  private int kamar_bayi;
  private int kamar_keluarga;

  public DataPenginapan(int id, String nama, String lokasi, int tipe, int harga,
      int kamar_dewasa,
      int kamar_bayi,
      int kamar_anak,
      int kamar_keluarga) {
    this.id = id;
    this.nama = nama;
    this.lokasi = lokasi;
    this.tipe = tipe;
    this.harga = harga;
    this.kamar_dewasa = kamar_dewasa;
    this.kamar_anak = kamar_anak;
    this.kamar_bayi = kamar_bayi;
    this.kamar_keluarga = kamar_keluarga;
  }

  public int getId() {
    return id;
  }

  public String getNama() {
    return nama;
  }

  public String getLokasi() {
    return lokasi;
  }

  public int getHarga() {
    return harga;
  }

  public int getTipe() {
    return tipe;
  }

  public int getKamarDewasa() {
    return kamar_dewasa;
  }

  public int getKamarBayi() {
    return kamar_bayi;
  }

  public int getKamarAnak() {
    return kamar_anak;
  }

  public int getKamarKeluarga() {
    return kamar_keluarga;
  }

  public boolean search(Object obj) {
    DataPenginapan dataPenginapan = (DataPenginapan) obj;

    return this.lokasi.equals(dataPenginapan.getLokasi()) && this.tipe == dataPenginapan.getTipe()
        && this.kamar_dewasa >= dataPenginapan.getKamarDewasa()
        && this.kamar_bayi >= dataPenginapan.getKamarBayi() && this.kamar_anak >= dataPenginapan.getKamarAnak()
        && this.kamar_keluarga >= dataPenginapan.getKamarKeluarga(); 
  }
}